import Gun from 'gun'
import * as readlineSync from 'readline-sync';

const gun = new Gun({
//	peers: [],
//	radisk: true,
//	file: 'file/path.json'
});

gun.get('person').on(() => {
	console.log(`a thing happened`);
});


	// wait for keyboard input 
	readlineSync.question('>> ');

	console.log(`about to call gun::put`);
	// now put to gun
	gun.get('person').put({
		name: 'William',
		text: 'this is some data',
		when: new Date().getTime()
	},
		(ack) => console.log(`callback triggered ${ack}`));

	console.log(`did call gun::put`);
